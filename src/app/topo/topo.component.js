(function() {
	'use strict';
	
	angular
		.module('topo')
		.component('topo', {
			templateUrl: '/app/topo/topo.template.html',
			controller: TopoCtrl
		})
		.factory('TopoFactory', TopoFactory)

	TopoCtrl.$inject = ['TopoFactory'];

	function TopoCtrl(TopoFactory) {
		var vm = this;

		// TopoFactory
		// 	.get_menu_items()
		// 	.then(function(response){
		// 		vm.menu_items = response.data;
		// 	})
		// 	.catch(function(respones){
		// 		console.log('Error on get menu items')
		// 	})
	}

	function TopoFactory($http) {
		var functions = {
			get_menu_items: get_menu_items
		}

		return functions;

		function get_menu_items() {
			return $http({
				method: 'GET',
				url: API_URL+'/wp-admin/admin-ajax.php?action=get_menu'
			})
		}
	}
})();
