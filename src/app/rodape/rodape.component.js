(function() {
	'use strict';
	
	angular
		.module('rodape')
		.component('rodape', {
			templateUrl: '/app/rodape/rodape.template.html',
			controller: RodapeCtrl
		})
		.factory('RodapeFactory', RodapeFactory)

	RodapeCtrl.$inject = ['RodapeFactory'];

	function RodapeCtrl(RodapeFactory) {
		var vm = this;

		// RodapeFactory
		// 	.get_menu_items()
		// 	.then(function(response){
		// 		vm.menu_items = response.data;
		// 	})
		// 	.catch(function(respones){
		// 		console.log('Error on get menu items')
		// 	})
	}

	function RodapeFactory($http) {
		var functions = {
			get_menu_items: get_menu_items
		}

		return functions;

		function get_menu_items() {
			return $http({
				method: 'GET',
				url: API_URL+'/wp-admin/admin-ajax.php?action=get_menu'
			})
		}
	}
})();
