(function() {
	'use strict';
	
	angular
		.module('portfolio')
		.factory('PortfolioFactory', PortfolioFactory)

	PortfolioFactory.$inject = ['$http'];
	function PortfolioFactory($http) {
		var functions = {
			get_portfolio: get_portfolio
		}

		return functions;

		function get_portfolio() {
			return $http({
				method: 'GET',
				url: 'json/portfolio.json'
			})
		}
	}
})();
