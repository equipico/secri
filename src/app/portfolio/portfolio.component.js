(function() {
	'use strict';
	
	angular
		.module('portfolio')
		.component('portfolio', {
			templateUrl: '/app/portfolio/portfolio.template.html',
			controller: PortfolioCtrl
		});

	PortfolioCtrl.$inject = ['PortfolioFactory'];

	function PortfolioCtrl(PortfolioFactory) {
		var vm = this;

		PortfolioFactory
			.get_portfolio()
			.then(function(response){
				vm.portfolios = response.data;
				console.log(vm.portfolios);
			})
			.catch(function(respones){
				console.log('Error on get menu items')
			})
	}
})();
