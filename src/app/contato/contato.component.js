(function() {
	'use strict';
	
	angular
		.module('contato')
		.component('contato', {
			templateUrl: '/app/contato/contato.template.html',
			controller: ContatoCtrl
		})
		.factory('ContatoFactory', ContatoFactory)

	ContatoCtrl.$inject = ['ContatoFactory'];

	function ContatoCtrl(ContatoFactory) {
		var vm = this;

		// ContatoFactory
		// 	.get_menu_items()
		// 	.then(function(response){
		// 		vm.menu_items = response.data;
		// 	})
		// 	.catch(function(respones){
		// 		console.log('Error on get menu items')
		// 	})
	}

	function ContatoFactory($http) {
		var functions = {
			get_menu_items: get_menu_items
		}

		return functions;

		function get_menu_items() {
			return $http({
				method: 'GET',
				url: API_URL+'/wp-admin/admin-ajax.php?action=get_menu'
			})
		}
	}
})();
