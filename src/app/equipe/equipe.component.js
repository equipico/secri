(function() {
	'use strict';
	
	angular
		.module('equipe')
		.component('equipe', {
			templateUrl: '/app/equipe/equipe.template.html',
			controller: EquipeCtrl
		})
		.factory('EquipeFactory', EquipeFactory)

	EquipeCtrl.$inject = ['EquipeFactory'];

	function EquipeCtrl(EquipeFactory) {
		var vm = this;

		// EquipeFactory
		// 	.get_menu_items()
		// 	.then(function(response){
		// 		vm.menu_items = response.data;
		// 	})
		// 	.catch(function(respones){
		// 		console.log('Error on get menu items')
		// 	})
	}

	function EquipeFactory($http) {
		var functions = {
			get_menu_items: get_menu_items
		}

		return functions;

		function get_menu_items() {
			return $http({
				method: 'GET',
				url: API_URL+'/wp-admin/admin-ajax.php?action=get_menu'
			})
		}
	}
})();
