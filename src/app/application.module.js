(function() {
	'use strict';
	
	angular
		.module('application', [
			// modules
			'ui.router',
			'mgcrea.ngStrap',
			'ngSanitize',
			'ngMessages',
			'ngMask',
			'ngCookies',
			'ngAnimate',

			// components
			'menu',
			'topo',
			'portfolio',
			'equipe',
			'contato',
			'rodape'
		])
		.config(Routes);

		// routes
		function Routes($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider){
			$urlRouterProvider.otherwise("/");

			$httpProvider.defaults.transformRequest = function(data){
				if (data === undefined) {
					return data;
				}
				return $.param(data);
			}

			// Now set up the states
			$stateProvider
				.state('home', {
					url: "/",
					template:  '<menu></menu><topo></topo><portfolio></portfolio><equipe></equipe><contato></contato><rodape></rodape>'
				})

				.state('pages', {
					url: "/:page",
					template: 'app/views/pages/index.html'
				})

			// configure html5 to get links working on jsfiddle
			// $locationProvider.html5Mode({
			// 	enabled: true,
			// 	requireBase: true
			// })
		}
})();
